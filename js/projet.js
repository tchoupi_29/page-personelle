window.onload = () => {
    for (let elem of document.getElementsByClassName("contenuBut")) {
        elem.style.display = "none"
    }
    document.getElementById("modeBut").onchange = (event) => {
        if (event.target.checked) {
            for (let elem of document.getElementsByClassName("contenuBut")) {
                elem.style.display = "flex"
            }
        } else {
            for (let elem of document.getElementsByClassName("contenuBut")) {
                elem.style.display = "none"
            }
        }
    }
}