FROM httpd

WORKDIR /usr/local/apache2/htdocs/
COPY ./ ./
RUN chmod -R 755 ./res